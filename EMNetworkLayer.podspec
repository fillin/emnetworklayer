#
# Be sure to run `pod lib lint EMNetworkLayer.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'EMNetworkLayer'
  s.version          = '1.6.5'
  s.summary          = 'iOS Network Layer used above Alamofire.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'iOS Network Layer used above Alamofire. Written in Swift.'

  s.homepage         = 'https://gitlab.com/fillin/emnetworklayer.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jeka-mel' => 'iosdeveloper.mail@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/fillin/emnetworklayer.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  s.swift_version = '5.0'
  
  s.source_files = 'EMNetworkLayer/Classes/**/*'
  
  
  # s.resource_bundles = {
  #   'EMNetworkLayer' => ['EMNetworkLayer/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'Foundation'
  s.dependency 'Alamofire', '~> 4'
end
