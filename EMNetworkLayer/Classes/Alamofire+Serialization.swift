import Foundation
import Alamofire

public struct SerializedResponse {
    public let info: [String: Any]?
    public let error: Error?
    public let statusCode: Int
    
    public var isSuccess: Bool {
        if self.error != nil { return false }
        return Int(roundf((Float(statusCode) / 100))) == 2
    }
}

public protocol Serializable {
    func serialized() -> SerializedResponse
}

extension DefaultDataResponse: Serializable {
    
    public func serialized() -> SerializedResponse {
        let statusCode = self.response?.statusCode ?? NSURLErrorUnknown
        do {
            guard let data = self.data else { throw APIError.noData }
            let json = try data.serialized()
            return SerializedResponse(info: json, error: error, statusCode: statusCode)
        } catch let e {
            return SerializedResponse(info: nil, error: error ?? e, statusCode: statusCode)
        }
    }
}

extension APIResponse: Serializable {
    
    public func serialized() -> SerializedResponse {
        do {
            guard let data = self.info else { throw APIError.noData }
            let json = try data.serialized()
            return SerializedResponse(info: json, error: error, statusCode: statusCode)
        } catch let e {
            return SerializedResponse(info: nil, error: error ?? e, statusCode: statusCode)
        }
    }
}

extension Serializable {
    
    public func serialized(in queue: DispatchQueue?, completion: @escaping (SerializedResponse) -> Void) {
        let current = OperationQueue.current?.underlyingQueue ?? .main
        if let q = queue {
            q.async {
                let s = self.serialized()
                current.sync { completion(s) }
            }
        } else {
            completion(serialized())
        }
    }
}

fileprivate extension Data {
    
    func serialized() throws -> [String: Any] {
        let obj = try JSONSerialization.jsonObject(with: self, options: .allowFragments)
        if let dict = obj as? [String: Any] {
            return dict
        } else if let arr = obj as? [Any] {
            return ["root": arr]
        }
        throw APIError.parsingFailed
    }
}


/// Used for HTTP headers dump
extension Dictionary where Key == String, Value == String {
    
    var simpleDescription: String {
        let list = self.compactMap { (key, value) -> String? in
            return "\"\(key)\": \(value)"
        }.joined(separator: ", ")
        return "[\(list)]"
    }
}
