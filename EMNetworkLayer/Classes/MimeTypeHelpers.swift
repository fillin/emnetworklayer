import Foundation
import MobileCoreServices.UTType

public extension String {
    
    static func mimeTypeFrom(uti: String) -> String {
        return mimeTypeFrom(fileExtensionString: uti)
    }
    
    private static func _utiRefFrom(mimeTypeString: String) -> Unmanaged<CFString>? {
        let mimeType = mimeTypeString as CFString
        guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mimeType, nil) else {
            return nil
        }
        return uti
    }
    
    static func utiFromFrom(mimeType: String) -> String? {
        guard let uti = _utiRefFrom(mimeTypeString: mimeType),
            let utiString = UTTypeCopyPreferredTagWithClass(uti.takeUnretainedValue(), kUTTagClassMIMEType) else {
                return nil
        }
        return utiString.takeUnretainedValue() as String
    }
    
    static func fileExtensionFrom(mimeTypeString: String) -> String? {
        guard let uti = _utiRefFrom(mimeTypeString: mimeTypeString),
            let fileExtension = UTTypeCopyPreferredTagWithClass(uti.takeUnretainedValue(), kUTTagClassFilenameExtension) else {
                return nil
        }
        return fileExtension.takeUnretainedValue() as String
    }
    
    static func mimeTypeFrom(fileExtensionString: String) -> String {
        let fe = fileExtensionString as CFString
        var result = "application/octet-stream"
        guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fe, nil)?.takeUnretainedValue(),
            let mimeType = UTTypeCopyPreferredTagWithClass (uti, kUTTagClassMIMEType) else {
                return result
        }
        result = mimeType.takeRetainedValue() as String
        return result
    }
}

public extension Data {
    
    var mimeType: String {
        #if swift(>=3.0)
        let binArr: [UInt8] = self.withUnsafeBytes {
            let ptr = $0.bindMemory(to: UInt8.self)
            if let addr = ptr.baseAddress {
                return Array(UnsafeBufferPointer<UInt8>(start: addr, count: self.count / MemoryLayout<UInt8>.size))
            }
            return []
        }
        #else
            let binArr = Array(UnsafeBufferPointer(start: UnsafePointer<UInt8>(self.bytes), count: self.length))
        #endif
        
        /** String are built according to:
         https://developer.apple.com/library/content/documentation/Miscellaneous/Reference/UTIRef/Articles/System-DeclaredUniformTypeIdentifiers.html
         */
        
        let defaultValue = "application/octet-stream"
        
        guard !binArr.isEmpty else { return defaultValue }
        
        switch (binArr[0]) {
        case 0xFF:
            return String.mimeTypeFrom(fileExtensionString: "jpeg") //"image/jpeg"
        case 0x89:
            return String.mimeTypeFrom(fileExtensionString: "png") //"image/png"
        case 0x47:
            return String.mimeTypeFrom(fileExtensionString: "gif") //"image/gif"
        case 0x49, 0x4D :
            return String.mimeTypeFrom(fileExtensionString: "tiff") //"image/tiff"
        case 0x25:
            return String.mimeTypeFrom(fileExtensionString: "pdf") //"application/pdf"
        case 0xD0:
            return "application/vnd"
        case 0x46:
            if binArr.prefix(4) == [0x46, 0x4F, 0x52, 0x4D] {
                return "image/iff"
            } else {
                return "text/plain"
            }
        default:
            if binArr.prefix(2) == [0x42, 0x4D] {
                return String.mimeTypeFrom(fileExtensionString: "bmp") //"com.microsoft.bmp"//"image/x-ms-bmp"
            }
            if binArr.prefix(4) == [0x38, 0x42, 0x50, 0x53] {
                return String.mimeTypeFrom(fileExtensionString: "psd") //"image/psd"
            }
            if binArr.prefix(4) == [0x52, 0x49, 0x46, 0x46] {
                return "image/webp"
            }
            if binArr.prefix(4) == [0x00, 0x00, 0x01, 0x00] {
                return String.mimeTypeFrom(fileExtensionString: "ico") //"com.microsoft.ico"//"image/vnd.microsoft.icon"
            }
            if binArr.prefix(12) == [0x00, 0x00, 0x00, 0x0c, 0x6a, 0x50, 0x20, 0x20, 0x0d, 0x0a, 0x87, 0x0a] {
                return String.mimeTypeFrom(fileExtensionString: "jp2") //"image/jp2"
            }
        }
        return defaultValue
        
        //TODO: Handle video filetypes
    }
}
