import Foundation
import Alamofire

public enum APIError: Error {
    case unknown
    case invalidURL
    case invalidMultipartFormData
    case parsingFailed
    case noData
    case requestTimeOut
}

public class APIResponse {
    public let info: Data?
    public let error: Error?
    public let api: APIProtocol
    public let statusCode: Int
    fileprivate init(info: Data?, error: Error?, api: APIProtocol, statusCode: Int = NSURLErrorUnknown) {
        self.info = info
        self.error = error
        self.api = api
        self.statusCode = statusCode
    }
}

public final class RequestskManager {
    
    public static let `default`: RequestskManager = RequestskManager()
    
    public private(set) var session = Alamofire.SessionManager.default
    
    public var configuration: URLSessionConfiguration {
        get {
            session.session.configuration
        }
        set {
            guard newValue != session.session.configuration else { return }
            if newValue.httpAdditionalHeaders == nil {
                newValue.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
            }
            session = Alamofire.SessionManager(configuration: newValue)
        }
    }
}

public extension APIProtocol {
    
    func sync() -> APIResponse {
        do {
            let request = try self.apiRequest()
            NotificationCenter.default.post(name: Notification.Name.API.WillStart, object: request.request)
            listener?.api(self, willStart: request.request, curl: request.debugDescription)
            let response = request.syncResponse()
            let ar = response.apiResponse(self)
            NotificationCenter.default.post(name: Notification.Name.API.DidComplete, object: ar)
            listener?.api(self, didFinishWith: response.response, data: response.data, error: response.error)
            return ar
        } catch let e {
            let ar = APIResponse(info: nil, error: e, api: self, statusCode: NSURLErrorBadServerResponse)
            NotificationCenter.default.post(name: Notification.Name.API.OnError, object: ar)
            listener?.api(self, error: e)
            return ar
        }
    }
    
    func async(_ closure: @escaping ((APIResponse) -> Void)) {
        do {
            let request = try self.apiRequest()
            NotificationCenter.default.post(name: Notification.Name.API.WillStart, object: request.request)
            listener?.api(self, willStart: request.request, curl: request.debugDescription)
            request.response { (response) in
                self.listener?.api(self, didFinishWith: response.response, data: response.data, error: response.error)
                let ar = response.apiResponse(self)
                NotificationCenter.default.post(name: Notification.Name.API.DidComplete, object: ar)
                closure(ar)
            }
        } catch let e {
            let ar = APIResponse(info: nil, error: e, api: self, statusCode: NSURLErrorBadServerResponse)
            listener?.api(self, error: e)
            NotificationCenter.default.post(name: Notification.Name.API.OnError, object: ar)
            closure(ar)
        }
    }
    
    func upload(_ progress: ((Progress) -> Void)? = nil, _ completion: @escaping ((APIResponse) -> Void)) {
        do {
            guard let fm = multipartForm else {
                throw APIError.invalidMultipartFormData
            }
            guard let url = url else {
                throw APIError.invalidURL
            }
            listener?.api(willStartUpload: self)
            RequestskManager.default.session.upload(
                multipartFormData: { (multipartFormData) in
                    fm.fill(multipartFormData: multipartFormData)
            },
                usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold,
                to: url,
                method: .post,
                headers: self.headers)
            { (encodingResult) in
                switch encodingResult {
                case .failure(let encodingError):
                    self.listener?.api(self, error: encodingError)
                    let ar = APIResponse(info: nil, error: encodingError, api: self, statusCode: NSURLErrorDownloadDecodingFailedToComplete)
                    NotificationCenter.default.post(name: Notification.Name.API.OnError, object: ar)
                    completion(ar)
                case .success(let request, _, _):
                    NotificationCenter.default.post(name: Notification.Name.API.WillStart, object: request.request)
                    if let p = progress {
                        request.uploadProgress(closure: p)
                    }
                    request.response(queue: .main) { (response) in
                        self.listener?.api(self, didFinishWith: response.response, data: response.data, error: response.error)
                        let ar = response.apiResponse(self)
                        NotificationCenter.default.post(name: Notification.Name.API.DidComplete, object: ar)
                        completion(ar)
                    }
                }
            }
        } catch let e {
            completion(APIResponse(info: nil, error: e, api: self, statusCode: NSURLErrorDataNotAllowed))
            return
        }
    }
}

public extension APIProtocol {
    
    func syncUpload<T: Decodable>(_ progress: ((Progress) -> Void)? = nil, timeout: DispatchTime) -> Swift.Result<T, Swift.Error> {
        let sem = DispatchSemaphore(value: 0)
        var apiResponse: APIResponse?
        upload(progress) { (response) in
            apiResponse = response
            sem.signal()
        }
        let wres = sem.wait(timeout: timeout)
        if wres == .timedOut {
            return Swift.Result<T, Swift.Error> {
                throw APIError.requestTimeOut
            }
        }
        guard let resp = apiResponse else {
            return Swift.Result<T, Swift.Error> { throw APIError.noData }
        }
        return Swift.Result<T, Swift.Error> { try resp.item(itemType: T.self) }
    }

    func apiRequest(encoding: ParameterEncoding = JSONEncoding.default) throws -> DataRequest {
        let method = self.method
        var params = (self.params?.isEmpty ?? true) ? nil : self.params
        guard var url = self.url else {
            throw APIError.invalidURL
        }
        if let tmp = url.addingURLParams(for: method, from: params) {
            params = nil
            url = tmp
        }
        return RequestskManager.default.session.request(url, method: method, parameters: params, encoding: encoding, headers: self.headers)
    }
}

public extension DefaultDataResponse {
    func apiResponse(_ api: APIProtocol) -> APIResponse {
        let responseError: Error? = {
            if let e = error { return e }
            if let status = response?.statusCode, let code = HTTPStatusCode(rawValue: status) {
                if ![.success, .redirection].contains(code.responseType) {
                    return code
                }
            }
            return nil
        }()
        return APIResponse(info: data, error: responseError, api: api, statusCode: response?.statusCode ?? NSURLErrorUnknown)
    }
}

public extension DataRequest {
    func syncResponse(timeOut to: DispatchTime? = nil) -> DefaultDataResponse {
        let semaphore = DispatchSemaphore(value: 0)
        var result: DefaultDataResponse!
        self.response(queue: DispatchQueue.global(qos: .default)) { response in
            result = response
            semaphore.signal()
        }
        _ = semaphore.wait(timeout: to ?? DispatchTime.distantFuture)
        return result
    }
}

extension APIResponse {
    public var isSuccess: Bool {
        if self.error != nil { return false }
        return Int(roundf((Float(statusCode) / 100))) == 2
    }
}

// MARK: - Extensions

public extension Dictionary where Key == String, Value: Any {
    func encodedFor(method: HTTPMethod) -> String? {
        guard method == .get else { return nil }
        var addons = [String]()
        for (key, value) in self {
            if let arr = value as? [String] {
                for itm in arr {
                    addons.append("\(key)=\(itm)")
                }
            } else {
                let v = String(describing: value)
                addons.append("\(key)=\(v)")
            }
        }
        if addons.count > 0 {
            let tmp = "?" + addons.joined(separator: "&")
            if let result = tmp.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                return result
            }
        }
        return nil
    }
}

public extension URL {
    func addingURLParams(for method: HTTPMethod, from params: [String: Any]?) -> URL? {
        if let encodedParams = params?.encodedFor(method: method) {
            return URL(string: absoluteString + encodedParams)
        }
        return nil
    }
}
